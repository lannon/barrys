# Barrys

Automatically register for a class at barrysbootcamp.com

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'barrys'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install barrys

## Usage

Default settings will attempt to register for Anya's Thursday 18:40 Hard CORE Abs. 

This class (and all other classes) become available at noon the week before the day of
the desired class.

To register for this class, run:

```ruby
Barrys::Agent.new.register
```

The following variable can be passed into the `Barry::Agent` constructor or set via setter
methods:

* username
* password
* site_id
* instructor_id
* class_type_id
* week_index
* user_agent_alias
* desired_class_time_regexp
* root_url

## Contributing

1. Fork it ( https://github.com/[my-github-username]/barrys/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
