# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'barrys/version'

Gem::Specification.new do |spec|
  spec.name          = "barrys"
  spec.version       = Barrys::VERSION
  spec.authors       = ["John Lannon"]
  spec.email         = ["john.lannon@livingsocial.com"]
  spec.summary       = %q{automate scraping / registration of barry's bootcamp site}
  spec.description   = %q{automate scraping / registration of barry's bootcamp site}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_dependency "mechanize"
end
