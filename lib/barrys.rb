require "barrys/version"
require "mechanize"

module Barrys
  USERNAME         = "jlannon@gmail.com"
  PASSWORD         = "analdouche" 
  SITE_ID          = 10 # Central London
  INSTRUCTOR_ID    = 98 # Anya
  CLASS_TYPE_ID    = 16 # Hardcore abs
  WEEK_INDEX       = 0 # current week
  USER_AGENT_ALIAS = 'Windows Chrome'
  DESIRED_CLASS_TIME_REGEXP = /08:40/
  ROOT_URL = "http://www.barrysbootcamp.com"
  # every class opens at noon 1 week in advance
  # it should be an abs class
  # abs and core

  class Agent

    attr_accessor :username, :password, :site_id, :instructor_id, :class_type_id, :week_index,
      :user_agent_alias, :desired_class_time_regexp, :root_url

    def initialize(opts = {})
      @logged_in                 = false
      @username                  = opts[:username]                  || USERNAME
      @password                  = opts[:password]                  || PASSWORD
      @site_id                   = opts[:site_id]                   || SITE_ID
      @instructor_id             = opts[:instructor_id]             || INSTRUCTOR_ID
      @class_type_id             = opts[:class_type_id]             || CLASS_TYPE_ID
      @week_index                = opts[:week_index]                || WEEK_INDEX
      @user_agent_alias          = opts[:user_agent_alias]          || USER_AGENT_ALIAS
      @desired_class_time_regexp = opts[:desired_class_time_regexp] || DESIRED_CLASS_TIME_REGEXP
      @root_url                  = opts[:root_url]                  || ROOT_URL
    end

    def register
      check_time!

      login if !logged_in?

      while true do
        check_time!
        desired_class = class_listing_page.links.select { |l| l.text =~ desired_class_time_regexp }[0]
        break if desired_class
      end

      if desired_class
        puts "found class. looking for spots."
        class_page = agent.get("#{root_url}#{desired_class.href}")
        #
        # look for treadmill links,  note the spotid in href and T-{whatever} in text
        # <a href="/reserve/index.cfm?action=Reserve.book&amp;classid=70242&amp;spotid=16" class="spotcell " id="spotcell16"><span>T-16</span></a>
        #
        tread_mill_spots = class_page.links.select {|l| l.text =~ /^T-/ }
        return if attempt_to_book_spot(tread_mill_spots)
        #
        # fallback on floor spot, note the spotid in href and F-{whatever} in text
        # <a href="/reserve/index.cfm?action=Reserve.book&amp;classid=70242&amp;spotid=26" class="spotcell floor" id="spotcell26"><span>F-6</span></a>
        #
        floor_spots = class_page.links.select {|l| l.text =~ /^F-/ }
        return if attempt_to_book_spot(floor_spots)
        #
        # fallback on dobule floor
        # <a href="/reserve/index.cfm?action=Reserve.book&amp;classid=70242&amp;spotid=45" class="spotcell floor doublefloor" id="spotcell45"><span>DF-25</span></a>
        #
        double_floor_spots = class_page.links.select {|l| l.text =~ /^DF-/ }
        return if attempt_to_book_spot(double_floor_spots)
        #
        # fallback on waitlist
        # <a href="/reserve/index.cfm?action=Reserve.bookWaitlist&amp;classid=70240">Add me to the waitlist</a>
        waitlist_spots = class_page.links.select { |l| l.href =~ /bookWaitlist/ }
        return if attempt_to_book_spot(waitlist_spots)
      else
        puts "unable to find desired class. fuck off."
      end
    end
    
    private

    def check_time!
      current_time = Time.now
      elevenish = current_time.hour == 11 && current_time.minute.between?(50,59)
      twelvish  = current_time.hour == 12 && current_time.minute.between?(0,10)
      unless elevenish || twelvish
        abort("not between 11:50 and 12:10. fuck off.")
      end
    end

    def attempt_to_book_spot(spots)
      book_spot(spots.sample) if spots.any?
    end

    def book_spot(spot)
      # a spot is a mechanize link with #text and #href methods
      # no idea how to actually book as i don't have any class credits
      # guessing there's some confirmation click required
      # TODO: figure this out
      # return true if succesfully booked
      puts "attempting to book spot: #{spot.text}"
      spot.click
      true
    end

    def class_listing_page
      agent.get(class_url)
    end

    def agent
      @agent ||= Mechanize.new { |agent|
        agent.user_agent_alias = user_agent_alias
      }
    end

    def class_url
      "#{root_url}/reserve/index.cfm?action=Reserve.chooseClass&site=#{site_id}&instructorid=#{instructor_id}&wk=#{week_index}&classtypeid=#{class_type_id}"
    end
    
    def login
      login_page = agent.get("http://www.barrysbootcamp.com/reserve/index.cfm?action=Account.login")
      login_form = login_page.form_with(name: 'loginForm')
      login_form.username = username 
      login_form.password = password
      login_form.click_button
      @logged_in = true
    end

    def logged_in?
      @logged_in
    end
  end
end
