require 'rubygems'
require 'barrys'

namespace :barrys do
  desc "register for class"
  task :register do
    Barrys::Agent.new.register
  end
end
